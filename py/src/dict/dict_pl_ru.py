import pickle
from multiprocessing import Pool

import time

import pymorphy2
import operator
from nltk.corpus import stopwords

def preprocess(sentence):
    res = [word[0] for word in sentence]
    return res

stopwords_pl = set(stopwords.words('polish'))
stopwords_ru = set(stopwords.words('russian'))

numbers_tag = ['numnp', 'numwordn', 'numn', 'numnn', 'numnnn', 'numnnnn', 'numnnnnn']

dict_tags = {('NOUN', 'NUMR'):('subst', 'depr', 'burk', 'ger', 'num'),
             ('ADJF', 'ADJS', 'COMP'): ('adj', 'adja', 'adjc', 'adv'),
             ('VERB', 'INFN'):('verb', 'ger'),
             ('PRTF', 'PRTS', 'GRND'):('pant', 'ppas', 'pcon'),
             ('NPRO',):('ppron12', 'ppron3', 'siebie'),
             ('ADVB',):('adv','adjp'),
             ('PRED',):('pred',),
             ('PREP',):('prep',),
             ('CONJ',):('conj', 'comp'),
             ('PRCL',):('qub',),
             ('INTJ',):('interj',)
             }


def remove_stop_words_pl(sentence):
    res = []
    for w in sentence:
        if w[0] not in stopwords_pl:
            res.append(w[0])
    return res


def remove_stop_words_ru(sentence):
    res = []
    for w in sentence:
        if w[0] not in stopwords_ru:
            res.append(w[0])
    return res


def remove_all_but_stop_words_pl(sentence):
    res = []
    for w in sentence:
        if w[0] in stopwords_pl:
            res.append(w[0])
    return res


def remove_all_but_stop_words_ru(sentence):
    res = []
    for w in sentence:
        if w[0] in stopwords_ru:
            res.append(w[0])
    return res


def load_stem(lang1, lang2):
    with open('../../data/temp/' + lang1 + '_stem_whole_txt.pkl', 'rb') as file:
        stem1 = pickle.load(file)

    with open('../../data/temp/' + lang1 + '_corp_ids.pkl', 'rb') as file:
        ids1 = pickle.load(file)

    with open('../../data/temp/' + lang2 + '_stem_whole_txt.pkl', 'rb') as file:
        stem2 = pickle.load(file)

    with open('../../data/temp/' + lang2 + '_corp_ids.pkl', 'rb') as file:
        ids2 = pickle.load(file)

    res1 = []
    res2 = []
    for k, v in ids1.items():
        k_l2 = k[:-2] + lang2
        if k_l2 in ids2:
            if "opensub" in k_l2:
                end = 0
                # 'opensub' data is excluded from train set
            else:
                res1.extend(stem1[v[0]:v[1] - end])
                v2 = ids2[k_l2]
                res2.extend(stem2[v2[0]:v2[1] - end])


    return res1, res2

def generate_dict(corpuse_pl, corpuse_ru, threshold=1):
    iidx_ru = {}
    iidx_pl = {}
    for i in range(0, len(corpuse_ru)):
        for w in corpuse_ru[i]:
            temp_iidx = iidx_ru.get(w, [])
            temp_iidx.append(i)
            iidx_ru[w] = temp_iidx

    for i in range(0, len(corpuse_pl)):
        for w in corpuse_pl[i]:
            temp_iidx = iidx_pl.get(w, [])
            temp_iidx.append(i)
            iidx_pl[w] = temp_iidx


    translation_dict_pl = {}
    for key, idx in iidx_ru.items():
        if len(idx) > threshold:
            temp = {}
            for sentence_id in idx:
                sentence = corpuse_pl[sentence_id]
                for w in sentence:
                    counter = temp.get(w, 0)
                    counter += 1
                    temp[w] = counter
            translation_dict_pl[key] = temp

    dict_pl_to_ru = {}
    for key, counter_dict in translation_dict_pl.items():
        temp = {}
        for k, v in counter_dict.items():
            temp[k] = v * v / (len(iidx_ru[key]) + len(iidx_pl[k]))

        if len(temp) > 1:
            translation = max(temp.items(), key=operator.itemgetter(1))[0]
            dict_pl_to_ru[key] = translation
    return dict_pl_to_ru


if __name__ == "__main__":
    start = time.time()
    print('v5')
    morph = pymorphy2.MorphAnalyzer()
    corpuse_pl, corpuse_ru = load_stem("pl", "ru")

    print(len(corpuse_pl))
    print(len(corpuse_ru))
    with Pool(processes=46) as pool:
        corpuse_pl = list(pool.map(remove_stop_words_pl, corpuse_pl))

    with Pool(processes=46) as pool:
        corpuse_ru = list(pool.map(remove_stop_words_ru, corpuse_ru))


    dict_pl_to_ru = generate_dict(corpuse_pl, corpuse_ru)
    print(len(dict_pl_to_ru))
    with open('../../data/temp/pl_to_ru_dict2.pkl', 'wb') as file:
        pickle.dump(dict_pl_to_ru, file)

    corpuse_pl, corpuse_ru = load_stem("pl", "ru")


    with Pool(processes=46) as pool:
        corpuse_pl = list(pool.map(remove_all_but_stop_words_pl, corpuse_pl))

    with Pool(processes=46) as pool:
        corpuse_ru = list(pool.map(remove_all_but_stop_words_ru, corpuse_ru))

    dict_pl_to_ru = generate_dict(corpuse_pl, corpuse_ru)
    print(len(dict_pl_to_ru))
    for key, v in dict_pl_to_ru.items():
        print(key + " => " + v)

    with open('../../data/temp/pl_to_ru_stopwords_dict2.pkl', 'wb') as file:
        pickle.dump(dict_pl_to_ru, file)

