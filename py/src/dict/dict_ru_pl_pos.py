import pickle
from multiprocessing import Pool

import time
import operator
from nltk.corpus import stopwords
import pymorphy2

def preprocess(sentence):
    res = [word[0] for word in sentence]
    return res

stopwords_pl = set(stopwords.words('polish'))
stopwords_ru = set(stopwords.words('russian'))

numbers_tag = ['numnp', 'numwordn', 'numn', 'numnn', 'numnnn', 'numnnnn', 'numnnnnn']
pl_main_tags = {'adj', 'adja', 'adjc', 'adjp', 'adv', 'burk', 'depr', 'ger', 'conj', 'comp', 'num', 'pact', 'pant',
                'pcon', 'ppas', 'ppron12', 'ppron3', 'pred', 'prep', 'siebie', 'subst', 'verb', 'brev', 'interj', 'qub',
                'NaN'}

dict_tags = {('NOUN', 'NUMR', None):('subst', 'depr', 'burk', 'ger', 'num', 'NaN'),
             ('ADJF', 'ADJS', 'COMP'): ('adj', 'adja', 'adjc', 'adv'),
             ('VERB', 'INFN', 'PRED'):('verb', 'ger', 'pred'),
             ('PRTF', 'PRTS', 'GRND'):('pant', 'pact', 'ppas', 'pcon'),
             ('NPRO',):('ppron12', 'ppron3', 'siebie'),
             ('ADVB',):('adv','adjp'),
             ('PREP',):('prep',),
             ('CONJ',):('conj', 'comp'),
             ('PRCL',):('qub',),
             ('INTJ',):('interj',)
             }

def print_m(*args):
    r = " "
    for a in args:
        r = r + " " + str(a)
    print(r)

def convert_pl_tags(tag):
    result_main = []
    if tag.startswith('num:comp'):
        tag = 'num'
    tags = tag.split("+")
    for tag in tags:
        tag = tag.split(":")
        res = []
        if len(tag) == 1:
            tag = tag[0]
            if tag != 'refl.nonrefl' and tag != 'imperf.perf' and '.' in tag:
                res.extend(tag.split('.'))
            else:
                res = [tag]
        else:
            for t in tag:
                if t != 'refl.nonrefl' and t != 'imperf.perf' and '.' in t:
                    res.extend(t.split('.'))
                else:
                    res.append(t)

        for t in res:
            if t in pl_main_tags:
                result_main.append(t)

    return result_main


def remove_stop_words_pl(sentence):
    res = []
    for w in sentence:
        if w[0] == 'się' or w[0] not in stopwords_pl:
            res.append(w)
    return res


def remove_stop_words_ru(sentence):
    res = []
    for w in sentence:
        if w[0] not in stopwords_ru:
            res.append(w)
    return res


def remove_all_but_stop_words_pl(sentence):
    res = []
    for w in sentence:
        if w[0] != 'nie' and w[0] in stopwords_pl:
            res.append(w)
    return res


def remove_all_but_stop_words_ru(sentence):
    res = []
    for w in sentence:
        if w[0] in stopwords_ru:
            res.append(w)
    return res


def load_stem(lang1, lang2):
    with open('../../data/temp/' + lang1 + '_stem_whole_txt.pkl', 'rb') as file:
        stem1 = pickle.load(file)

    with open('../../data/temp/' + lang1 + '_corp_ids.pkl', 'rb') as file:
        ids1 = pickle.load(file)

    with open('../../data/temp/' + lang2 + '_stem_whole_txt.pkl', 'rb') as file:
        stem2 = pickle.load(file)

    with open('../../data/temp/' + lang2 + '_corp_ids.pkl', 'rb') as file:
        ids2 = pickle.load(file)

    res1 = []
    res2 = []
    for k, v in ids1.items():
        k_l2 = k[:-2] + 'ru'
        if k_l2 in ids2:
            if "opensub" in k_l2:
                end = 0
                # 'opensub' data is excluded from train set
            else:
                res1.extend(stem1[v[0]:v[1] - end])
                v2 = ids2[k_l2]
                res2.extend(stem2[v2[0]:v2[1] - end])


    return res1, res2


def generate_dict(corpuse_pl, corpuse_ru, tags_ru, tags_pl, threshold = 1):
    iidx_ru = {}
    iidx_pl = {}
    tags_pl = set(tags_pl)
    tags_ru = set(tags_ru)
    sie_allowed = 'VERB' in tags_ru or 'GRND' in tags_ru
    for i in range(0, len(corpuse_ru)):
        for w in corpuse_ru[i]:
            if w[1].POS in tags_ru:
                temp_iidx = iidx_ru.get(w[0], [])
                temp_iidx.append(i)
                iidx_ru[w[0]] = temp_iidx

    for i in range(0, len(corpuse_pl)):
        sentence = corpuse_pl[i]
        for j in range(0, len(sentence)):
            w = sentence[j]
            tags = convert_pl_tags(w[1])
            if any(x in tags for x in tags_pl):
                temp_iidx = iidx_pl.get(w[0], [])
                temp_iidx.append(i)
                iidx_pl[w[0]] = temp_iidx
                if sie_allowed and j+1 < len(sentence) and sentence[j + 1][0] == 'się':
                    wk = w[0] + ' się'
                    temp_iidx = iidx_pl.get(wk, [])
                    temp_iidx.append(i)
                    iidx_pl[wk] = temp_iidx

    iidx_pl['się'] = []

    translation_dict_ru = {}
    for key, idx in iidx_ru.items():
        if len(idx) > threshold:
            temp = {}
            for sentence_id in idx:
                sentence = corpuse_pl[sentence_id]
                for j in range(0, len(sentence)):
                    w = sentence[j]
                    if w[0] != 'się':
                        tags = convert_pl_tags(w[1])
                        if any(x in tags for x in tags_pl):
                            counter = temp.get(w[0], 0)
                            counter += 1
                            temp[w[0]] = counter
                            if sie_allowed and j + 1 < len(sentence) and sentence[j + 1][0] == 'się':
                                wk = w[0] + ' się'
                                counter = temp.get(wk, 0)
                                counter += 1
                                temp[wk] = counter
            translation_dict_ru[key] = temp

    dict_ru_to_pl = {}
    for key, counter_dict in translation_dict_ru.items():
        temp = {}
        for k, v in counter_dict.items():
            temp[k] = v * v / (len(iidx_ru[key]) + len(iidx_pl[k]))

        if len(temp) > 1:
            translation = max(temp.items(), key=operator.itemgetter(1))[0]
            dict_ru_to_pl[key] = translation
    return dict_ru_to_pl


if __name__ == "__main__":
    start = time.time()
    print('v4')
    morph = pymorphy2.MorphAnalyzer()
    corpuse_pl, corpuse_ru = load_stem("pl", "ru")
    print_m(len(corpuse_pl), len(corpuse_ru))

    with Pool(processes=46) as pool:
        corpuse_pl = list(pool.map(remove_stop_words_pl, corpuse_pl))

    with Pool(processes=46) as pool:
        corpuse_ru = list(pool.map(remove_stop_words_ru, corpuse_ru))

    dict_ru_to_pl = {}
    for tags_ru, tags_pl in dict_tags.items():
        dict_ru_to_pl[tags_ru] = generate_dict(corpuse_pl, corpuse_ru, tags_ru, tags_pl)



    print(len(dict_ru_to_pl))

    with open('../../data/temp/ru_to_pl_dict_pos2.pkl', 'wb') as file:
        pickle.dump(dict_ru_to_pl, file)



    #STOPWORDS -----------------------------------------------------------------
    corpuse_pl, corpuse_ru = load_stem("pl", "ru")


    with Pool(processes=46) as pool:
        corpuse_pl = list(pool.map(remove_all_but_stop_words_pl, corpuse_pl))

    print(corpuse_pl[0:10])
    with Pool(processes=46) as pool:
        corpuse_ru = list(pool.map(remove_all_but_stop_words_ru, corpuse_ru))

    print(corpuse_ru[0:10])
    dict_ru_to_pl = {}
    for tags_ru, tags_pl in dict_tags.items():
        dict_ru_to_pl[tags_ru] = generate_dict(corpuse_pl, corpuse_ru, tags_ru, tags_pl)

    for t, d in dict_ru_to_pl.items():
        print(t)
        i = 0
        for k, v in d.items():
            print_m(k, " => ", v)
            i += 1


    print(len(dict_ru_to_pl))

    with open('../../data/temp/ru_to_pl_stopwords_pos2.pkl', 'wb') as file:
        pickle.dump(dict_ru_to_pl, file)
