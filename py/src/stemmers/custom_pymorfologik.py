# coding=utf-8

import os
import subprocess
from collections import OrderedDict, defaultdict


class BaseParser(object):
    def parse(self, output, **kwargs):
        """Parse output of the morfologik"""
        return
    parse.__annotations__ = {'output': str}

    def _get_lines_with_stems(self, output):
        """
        Removes first four lines (morfologik run params description) as well as
        the last line (performance information).
        """
        return output.split('\n')[:-1]
    _get_lines_with_stems.__annotations__ = {'output': str, 'return': list}




class DictParser(BaseParser):
    """
    DictParser parses the morfologik output string to produce a dict with
    stems for output words. Each output word appears only once in the resulting
    dict.
    """
    def _make_unique(self, words):
        keys = OrderedDict()
        for e in words:
            keys[e] = 1
        return keys.keys()

    def _parse_for_simple_stems(self, lines):
        stems = defaultdict(lambda: [])

        for line in lines:
            original_word, stem, _ = line.split("\t")

            if stem == '-':
                continue

            stems[original_word].append(stem)
        return dict(stems)

    def parse(self, output):
        """
        Find stems for a given text.
        """
        output = self._get_lines_with_stems(output)
        words = self._make_unique(output)
        return self._parse_for_simple_stems(words)
    parse.__annotations__ = {'output': list, 'return': dict}


class ListParser(BaseParser):
    """
    ListParser parses the morfologik output string to produce a list of tuples
    with stems for each output word. Each output word may appear several times
    in the resulting list.
    """
    def __init__(self, syntaxsplit_word):
        self.syntaxsplit = syntaxsplit_word

    def parse(self, output, skip_empty=False, skip_same=True):
        lines_with_stems = self._get_lines_with_stems(output)

        stems = list()
        last_word = None

        for line in lines_with_stems:
            word, stem, part = line.split("\t")
            stem = stem if stem != '-' else None

            if skip_empty and (stem is None):
                continue

            if word == self.syntaxsplit or last_word != word:
                stems.append((word, {}))

            stem = None if skip_same and stem in stems[-1][1].keys() else stem
            if stem is not None:
                if stem not in stems[-1][1].keys():
                    stems[-1][1][stem] = [part]
                else:
                    if part not in stems[-1][1][stem]:
                        stems[-1][1][stem].append(part)

            last_word = word

        return stems
    parse.__annotations__ = {'output': str,
                             'skip_empty': bool, 'skip_same': bool,
                             'return': list}


class Morfologik(object):
    def __init__(self, jar_path=''):
        self.jar_path = jar_path

        this_dir, _ = os.path.split(__file__)
        if len(this_dir) == 0:
            self.jar_path = os.path.join("./../lib/morfologik-2.1", "morfologik-wrapper-mt.jar")
        else:
            self.jar_path = os.path.join(this_dir + "/../../lib/morfologik-2.1", "morfologik-wrapper-mt.jar")

    __init__.__annotations__ = {'jar_path': str}

    def stem(self, words, parser, **kwargs):
        """
        Get stems for the words using a given parser

        Example:
            from .parsing import ListParser

            parser = ListParser()
            stemmer = Morfologik()
            stemmer.stem(['ja tańczę a ona śpi], parser)

            [
                 ('ja': ['ja']),
                 ('tańczę': ['tańczyć']),
                 ('a': ['a']),
                 ('ona': ['on']),
                 ('śpi': ['spać'])
            ]
        """
        output = self._run_morfologik(words)
        return parser.parse(output, **kwargs)

    stem.__annotations__ = {'words': list, 'parser': BaseParser}

    def _run_morfologik(self, words):
        """
        Runs morfologik java jar and assumes that input and output is
        UTF-8 encoded.
        """
        p = subprocess.Popen(
            ['java', '-Dfile.encoding=UTF-8', '-jar', self.jar_path,
             '-ie', 'UTF-8',
             '-oe', 'UTF-8'],
            bufsize=-1,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT)
        out, _ = p.communicate(input=bytes("\n".join(words), "utf-8"))
        return out.decode("utf-8")

    _run_morfologik.__annotations__ = {'words': list, 'return': str}

    def _make_unique(self, words):
        keys = OrderedDict()
        for e in words:
            keys[e] = 1
        return keys.keys()

