import pickle
import re
import time

from gensim.utils import simple_preprocess
from src.stemmers.custom_pymorfologik import Morfologik, ListParser

syntax_split_keyword = 'syntaxsplit'


def translate_number(org_sentence):
    res = re.sub(r'\b\d+[.,!@#$%^&*()]+\d+\b', 'numnp', org_sentence)
    res = re.sub(r'([A-Za-z]+[\d@]+[\w@]*|[\d@]+[A-Za-z]+[\w@]*)', 'numwordn', res)
    res = re.sub(r'\b\d+\b', 'numn', res)

    return res

def preprocess(text):
    text = translate_number(text)
    text = simple_preprocess(text, max_len=100, min_len=1)
    res = stem(text)

    return res


def stem(word):
    val_stemmed = stemmer.stem(word, parser)
    res = []
    for v in val_stemmed:
        if len(v[1]) > 0:
            if 'mieć' in v[1]:
                key = 'mieć'
            else:
                key = next(iter(v[1]))
            value = v[1][key][0]
            res.append((key, value))
        else:
            res.append((v[0], 'NaN'))
    return res

def prepare_data(corpuse):
    res = (' ' + syntax_split_keyword + ' ').join(corpuse)

    return res

def decode_prepare_data(corpuse):
    doc = []
    res = []
    for word in corpuse:
        if word[0] == syntax_split_keyword:
            res.append(doc)
            doc = []
        else:
            doc.append(word)

    res.append(doc)
    return res


if __name__ == "__main__":
    start = time.time()
    parser = ListParser(syntaxsplit_word=syntax_split_keyword)
    stemmer = Morfologik()

    with open('../../data/temp/pl_whole_txt.pkl', 'rb') as file:
        corpuse = pickle.load(file)

    corpuse = prepare_data(corpuse)
    corpuse = preprocess(corpuse)
    corpuse = decode_prepare_data(corpuse)

    with open('../../data/temp/pl_stem_whole_txt.pkl', 'wb') as file:
        pickle.dump(corpuse, file)

    print("execution polish data processing took: " + "{:8.4f}".format(time.time() - start) + " for " + str(len(corpuse)) + " docs; avg time per doc = " + "{:4.6f}".format((time.time() - start) / len(corpuse)))
