import pickle
from multiprocessing import Pool

from gensim.utils import simple_preprocess
import pymorphy2
import time
import re

def translate_number(org_sentence):
    res = re.sub(r'\b\d+[.,!@#$%^&*()]+\d+\b', 'numnp', org_sentence)
    res = re.sub(r'([A-Za-z]+[\d@]+[\w@]*|[\d@]+[A-Za-z]+[\w@]*)', 'numwordn', res)
    res = re.sub(r'\b\d+\b', 'numn', res)

    return res

def preprocess(text):
    text = translate_number(text)
    res = simple_preprocess(text, max_len=100, min_len=1)
    res = [stem(x) for x in res]

    return res


def stem(word):
    val_stemmed = morph.parse(word)[0]
    return (val_stemmed[2], val_stemmed[1])


if __name__ == "__main__":
    start = time.time()
    morph = pymorphy2.MorphAnalyzer()
    with open('../../data/temp/ru_whole_txt.pkl', 'rb') as file:
        corpuse = pickle.load(file)

    with Pool(processes=32) as pool:
        corpuse = list(pool.map(preprocess, corpuse))


    with open('../../data/temp/ru_stem_whole_txt.pkl', 'wb') as file:
        pickle.dump(corpuse, file)

    print("execution russian data processing took: " + "{:8.4f}".format(time.time() - start) + " for " + str(len(corpuse)) + " docs; avg time per doc = " + "{:4.6f}".format((time.time() - start) / len(corpuse)))



